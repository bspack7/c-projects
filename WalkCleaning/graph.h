//
// Created by Brett Spackman on 4/21/18.
//

#ifndef CS3HW7_GRAPH_H
#define CS3HW7_GRAPH_H

#include "edge1.h"
#include <vector>

class graph {
public:

  graph(std::string fileName);

  std::vector<Edge> edgesVec;
  std::vector<int> cycles;

  int size;
  int edges;
  int cycleID = 0;

  int **adjMatrix;

  bool isEvenDegree();

  void markCycles();
  void recursiveMarkCycles(int pos, int start, int curr);

  void joinCycles();
  // void recursiveJoinCycles(Edge viewEdge);
  void notrecursiveJoinCycles();


};


#endif //CS3HW7_GRAPH_H
