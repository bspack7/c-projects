cmake_minimum_required(VERSION 3.8)
project(cs3hw7)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES TestEuler.cpp edge1.h graph.cpp graph.h)
add_executable(cs3hw7 ${SOURCE_FILES})