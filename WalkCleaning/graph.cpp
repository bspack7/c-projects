//
// Created by Brett Spackman on 4/21/18.
//

#include <fstream>
#include <iostream>
#include "graph.h"

graph::graph(std::string fileName) { // constructor that will take the file, assign variables, build vector & matrix

  Edge edge; // creates a new edge
  char from; // will get the first letter of edge from txt
  char to; // will get the second letter of edge from txt
  int count = 0; // will keep track of location put into vector:edges

  ifstream readFile;
  readFile.open(fileName);
  readFile >> size >> edges; // assigns the first two numbers of the file
  // size is how many letters, edges is amount of connections

  // using double pointers I can create a 2d array
  adjMatrix = new int *[size]; // creates columns of "size"
  for (int i = 0; i < size; i++) {
    adjMatrix[i] = new int[size]; // creates rows of "size"
    for (int j = 0; j < size; j++) {
      adjMatrix[i][j] = -1; // initializes all elements in adjMatrix to -1
    }
  } // end up with a size x size 2d array

  while (readFile>>from >>to) // read file until I close it
  {
    //readFile >> from >> to; // assign first letter to "from", second to "to"
    edge.fromNode = from;
    edge.toNode = to; // edge elements are updated
    edge.cycleID = -1;
    edge.used = false;
    edgesVec.push_back(edge); // put that edge into my vector
    count++; // increment count (size of vector)
    adjMatrix[edge.toNode - 65][edge.fromNode - 65] = count; // put count into position in matrix (AB = count)
    adjMatrix[edge.fromNode - 65][edge.toNode - 65] = count; // (BA = count)
  }
  readFile.close(); // no more reading to be down in the file, close it, vector and adjMatrix have been filled

  std::cout << "Edges Vector: \n";

  for (int i = 0; i < size; i++) {
    std::cout << "[" << edgesVec[i].fromNode - 'A' << " " << edgesVec[i].toNode - 'A' << "] " << std::endl;
  }

  std::cout << std::endl << "Adjacency Matrix of " << fileName << ":\n";

  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      std::cout << adjMatrix[i][j] << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;

  if (!isEvenDegree()) {
    std::cout << "No Euler Tour Available" << std::endl;
  }

  markCycles();
  if (fileName != "prog7C.txt")
  {
    joinCycles();
  }
}

bool graph::isEvenDegree() {
  int degreeCount = 0;
  for (int i = 0; i < size; i++) {
    degreeCount = 0;
    for (int j = 0; j < size; j++) {
      if (adjMatrix[i][j] > 0) {
        degreeCount++;
      }
    }
    if (degreeCount % 2 == 1) {
      return false;
    } else {
      return true;
    }
  }
}

void graph::markCycles() {
  for (int i = 0; i < edgesVec.size(); i++)
  {
    if (edgesVec[i].cycleID < 0)
    {
      int pos = i;
      edgesVec[i].cycleID = cycleID;
      int start = edgesVec[i].fromNode;
      int curr = edgesVec[i].toNode;
      std::cout << "Cycle #" << cycleID << ":" << std::endl;
      char startC = start;
      char currC = curr;
      std::cout << startC << "->" << currC;
      recursiveMarkCycles(pos, start, curr);
      std::cout << std::endl;
      cycleID++;
    }
  }

}

void graph::recursiveMarkCycles(int pos, int start, int curr) // pos is the node we are checking, parent is keeping track of start and end
{
  if ((edgesVec[pos].toNode == start || edgesVec[pos].fromNode == start) && (edgesVec[pos].fromNode != start && edgesVec[pos].toNode != curr))
  {
    return;
  }
  int currNode = edgesVec[pos].toNode;
  for (int i = 0; i < edgesVec.size(); ++i) {
    if (pos != i && edgesVec[i].cycleID < 0 && currNode == edgesVec[i].fromNode)
    {
      edgesVec[i].cycleID = cycleID;
      char to = edgesVec[i].toNode;
      std::cout << "->" << to;
      recursiveMarkCycles(i, start, curr);
      return;
    }
    else if (pos != i && edgesVec[i].cycleID < 0 && currNode == edgesVec[i].toNode)
    {
      edgesVec[i].cycleID = cycleID;
      int temp = edgesVec[i].toNode;
      edgesVec[i].toNode = edgesVec[i].fromNode;
      edgesVec[i].fromNode = temp;
      char to = edgesVec[i].toNode;
      std::cout << "->" << to;
      recursiveMarkCycles(i, start, curr);
      return;
    }
  }
}

void graph::joinCycles() {
  if (cycleID - 1 == 0) // cycle was 1 more than I needed
  {
    std::cout << std::endl << "No cycle joining was necessary." << std::endl;
    return;
  }
//  edgesVec[0].used = true; // starting edge
//  cycles.push_back(edgesVec[0].cycleID);
//  Edge viewEdge = edgesVec[0];
  notrecursiveJoinCycles();
  std::cout << std::endl << std::endl;
}

//void graph::recursiveJoinCycles(Edge viewEdge)
//{
//  std::vector<int> potentialMoves;
//
//  if (cycles.size() == 0) // base case
//  {
//    return;
//  }
//
//  for (int i = 0; i < edgesVec.size(); i++) // find all the unused connected edges
//  {
//    bool same = (edgesVec[i].toNode == viewEdge.toNode && edgesVec[i].fromNode == viewEdge.fromNode);
//    if (!same && !edgesVec[i].used && viewEdge.toNode == edgesVec[i].fromNode)
//    {
//      // its not the same edge, hasn't been used, and it's touching my viewEdge
//      potentialMoves.push_back(i);
//    }
//    else if (!same && !edgesVec[i].used && viewEdge.toNode == edgesVec[i].toNode)
//    {
//      potentialMoves.push_back(i);
//    }
//    else if (!same && !edgesVec[i].used && viewEdge.fromNode == edgesVec[i].fromNode)
//    {
//      potentialMoves.push_back(i);
//    }
//    else if (!same && !edgesVec[i].used && viewEdge.fromNode == edgesVec[i].toNode)
//    {
//      potentialMoves.push_back(i);
//    }
//  }
//
//
//  for (int i = 0; i < potentialMoves.size(); i++) // check to see if we have a different cycle
//  {
//    if (edgesVec[potentialMoves[i]].cycleID != viewEdge.cycleID && std::find(cycles.begin(), cycles.end(), edgesVec[potentialMoves[i]].cycleID) == cycles.end()) // pick an edge who has a different cycles
//    {
//      cycles.push_back(edgesVec[potentialMoves[i]].cycleID);
//      edgesVec[potentialMoves[i]].used = true;
//      std::cout << (char)edgesVec[potentialMoves[i]].fromNode << "->" << (char)edgesVec[potentialMoves[i]].toNode;
//      recursiveJoinCycles(edgesVec[potentialMoves[i]]);
//    }
//  }
//  for (int i = 0; i < potentialMoves.size(); ++i)
//  {
//      if (edgesVec[potentialMoves[i]].cycleID == cycles[cycles.size()-1] && !edgesVec[potentialMoves[i]].used) // it's in the cycle and it hasn't been used
//      {
//        //cycles.push_back(potentialMoves[i].cycleID);
//        std::cout << (char)edgesVec[potentialMoves[i]].fromNode << "->" << (char)edgesVec[potentialMoves[i]].toNode;
//        //see if im the last one
//        bool allUsed = true;
//        for (int j = 0; j < edgesVec.size(); j++)
//        {
//          if (!edgesVec[j].used && edgesVec[j].cycleID == cycles[cycles.size()-1])
//          {
//            allUsed = false;
//          }
//        }
//        if (allUsed == true)
//        {
//          cycles.pop_back();
//        }
//        recursiveJoinCycles(edgesVec[potentialMoves[i]]);
//      }
//  }
//
//
//}

void graph::notrecursiveJoinCycles()
{
  edgesVec[0].used = true; // starting edge
  cycles.push_back(edgesVec[0].cycleID);
  Edge viewEdge = edgesVec[0];
  std::cout << std::endl << "Tour after joining: " << (char)edgesVec[0].toNode;

  while (cycles.size() > 0) // base case
  {
    std::vector<int> potentialMoves;

    for (int i = 0; i < edgesVec.size(); i++) // find all the unused connected edges
    {
      bool same = (edgesVec[i].toNode == viewEdge.toNode && edgesVec[i].fromNode == viewEdge.fromNode);
      if (!same && !edgesVec[i].used && viewEdge.toNode == edgesVec[i].fromNode)
      {
        // its not the same edge, hasn't been used, and it's touching my viewEdge

        potentialMoves.push_back(i);
      }
      else if (!same && !edgesVec[i].used && viewEdge.toNode == edgesVec[i].toNode)
      {

        potentialMoves.push_back(i);
      }
      else if (!same && !edgesVec[i].used && viewEdge.fromNode == edgesVec[i].fromNode)
      {
        potentialMoves.push_back(i);
      }
      else if (!same && !edgesVec[i].used && viewEdge.fromNode == edgesVec[i].toNode)
      {
        potentialMoves.push_back(i);
      }
    }

    bool foundNewGuy = false;
    for (int i = 0; i < potentialMoves.size(); i++) // check to see if we have a different cycle
    {
      if (std::find(cycles.begin(), cycles.end(), edgesVec[potentialMoves[i]].cycleID) == cycles.end()) // pick an edge who has a different cycles
      {
        cycles.push_back(edgesVec[potentialMoves[i]].cycleID);
        edgesVec[potentialMoves[i]].used = true;
        //
        if (viewEdge.toNode == edgesVec[potentialMoves[i]].fromNode){
          std::cout << "->" << (char)edgesVec[potentialMoves[i]].fromNode;
        }
        else{
          std::cout << "->" << (char)edgesVec[potentialMoves[i]].fromNode;
        }

        viewEdge = edgesVec[potentialMoves[i]];
        foundNewGuy = true;
        //recursiveJoinCycles(edgesVec[potentialMoves[i]]);
      }
    }
    if (!foundNewGuy){
      for (int i = 0; i < potentialMoves.size(); ++i)
      {
        if (edgesVec[potentialMoves[i]].cycleID == cycles[cycles.size()-1] && !edgesVec[potentialMoves[i]].used) // it's in the cycle and it hasn't been used
        {
          //cycles.push_back(potentialMoves[i].cycleID);
          //
          if (viewEdge.toNode == edgesVec[potentialMoves[i]].fromNode){
            std::cout << "->" << (char)edgesVec[potentialMoves[i]].fromNode;
          }
          else{
            std::cout << "->" << (char)edgesVec[potentialMoves[i]].toNode << "->" << (char)edgesVec[potentialMoves[i]].fromNode;
          }
          edgesVec[potentialMoves[i]].used = true;
          viewEdge = edgesVec[potentialMoves[i]];
          //see if im the last one
          bool allUsed = true;
          for (int j = 0; j < edgesVec.size(); j++)
          {
            if (!edgesVec[j].used && edgesVec[j].cycleID == cycles[cycles.size()-1])
            {
              allUsed = false;
            }
          }
          if (allUsed == true)
          {
            cycles.pop_back();
          }
          //recursiveJoinCycles(edgesVec[potentialMoves[i]]);
        }
      }
    }

  }


}

// couldn't get this function to word :(

//void graph::markCycles(Edge checkEdge, int cycleID) {
//  if (allEdgesMarked) {
//    return;
//  }
//  checkEdge.cycleID = cycleID;
//  cycles.push_back(checkEdge);
//  checkEdge.used = true;
//  for (int i = 0; i < edgesVec.size(); i++) {
//    edgesVec[i];
//    if (checkEdge.toNode == edgesVec[i].fromNode && !edgesVec[i].used) {
//      edgesVec[i].cycleID = cycleID;
//      cycles.push_back(edgesVec[i]);
//      edgesVec[i].used = true;
//      return markCycles(edgesVec[i], cycleID); // recursion call
//    }
//  }
//  for (int i = 0; i < edgesVec.size(); i++) {
//    edgesVec[i] = edgesVec[i];
//    if (checkEdge.toNode == edgesVec[i].toNode && !edgesVec[i].used) {
//      edgesVec[i].cycleID = cycleID;
//      cycles.push_back(edgesVec[i]);
//      edgesVec[i].used = true;
//      return markCycles(edgesVec[i], cycleID); // recursion call
//    }
//  }
//  for (int i = 0; i < edgesVec.size(); i++) {
//    if (!edgesVec[i].used) {
//      cycleID++;
//      return markCycles(edgesVec[i], cycleID); // recursion call
//    }
//  }
//  allEdgesMarked = true;
//
//  for (int i = 0; i < cycles.size(); i++) {
//    std::cout << "[" << cycles[i].fromNode - 'A' << "][" << cycles[i].toNode - 'A' << "] " << "C: " << cycles[i].cycleID
//              << std::endl;
//  }
//}



