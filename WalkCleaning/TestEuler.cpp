#include <iostream>
#include <iomanip>
#include "graph.h"
#include <assert.h>
#include <fstream>

using namespace std;

int main ()
{
//  ofstream fout;
//	fout.open("tourOut.txt");
//	assert(fout);

  cout << "PROG 7A" << endl << endl;

  graph g("prog7A.txt");
   // g.computeTour(fout);  // If I want the output to appear on console, I just make the parameter "cout"

  cout << endl << "PROG 7B" << endl << endl;

  graph g1("prog7B.txt");
  // g1.computeTour(fout);

  cout << "PROG 7C" << endl;

  graph g2("prog7C.txt");
   // g2.computeTour(cout);

  return 0;
}