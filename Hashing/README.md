This program was surprisingly efficient at writing a poem based on
what word usually came after the first. Although as humans we have
memory of hearing a poem such as Sam I am. So we could look at a
list of "next words" and make a more educated guess on what may
next. Another obvious advantage that a human may have is that we,
unlike machines, know english. Although poems may not always make
perfect since our language skills would give us another great way
to make an educated guess on what may come next in poem. A lot of
poems rhyme and this language model is not able to determine if a
words rhymes. A third thing that if a computer knew would allow it
to make a more accurate poem.

Depending on the structure of the poem some are more coherent than
others. Especially if there is minimal repetition in the poem. For
example, in "Green Eggs and Ham" there is so much repetition there
are many potential next words that are created. Compared to the
sentence at the top of inch about Computer Science, which only has
one option for next word. It knows that it has to be the next word
if there is only one word in the next word's vector.