#include <iostream>
#include <fstream>
#include <string>

#include "HashTable.h"
#include "NextWord.h"

using namespace std;


void clean(std::string & nextToken)
{
  for (int i = 0; i < nextToken.length();)
  {
    if (nextToken[i] >255 || nextToken[i] < 0 || ispunct(nextToken[i]))
      nextToken.erase(i, 1);
    else {
      nextToken[i] = tolower(nextToken[i]);
      i++;
    }
  }
}

vector<string> readPoem (string fileName)
{
  vector<string> allWords;
  string word;
  ifstream inputFile;
  inputFile.open(fileName);
  while(!inputFile.eof())
  {
    inputFile >> word;
    clean(word);

    if (word != "")
    {
      allWords.push_back(word);
    }

    // first part needs to be the word, second part the record (words following word)
  };

  inputFile.close();

  return allWords;
}

void fillHash (HashTable<string,FirstWordInfo> & table, vector<string> allWords)
{
  for (int i = 0; i < allWords.size(); i++)
  {
    FirstWordInfo * firstWord = table.find(allWords[i]);
    string nextWord;

    if (i+1 < allWords.size())
    {
      nextWord = allWords[i + 1];
    }

    bool foundWord = false;

    if (firstWord != NULL) // I find the "first word" in my hash
    {
      // the word in my vector is located in my table already, count how many times that word has come up
      firstWord->incrementCount();

      // check to see if the found word's next word is already in its nextword vector
      for (int j = 0; j < firstWord->secondWordList.size(); j++)
      {
        if (nextWord != "" && firstWord->secondWordList[j]->word == nextWord)
        {
          firstWord->secondWordList[j]->increment();
          foundWord = true;
        }
      }
      if (nextWord != "" && !foundWord)
      {
        NextWord* word = new NextWord(nextWord,1);
        firstWord->secondWordList.push_back(word);
      }
    }

    else // I don't find the "first word" in my hash
    {
      FirstWordInfo * newWord = new FirstWordInfo(allWords[i],1); // create first word of the word that wasn't found
      table.insert(allWords[i], newWord); // add the new first word to my allWords vector

      NextWord * newNextWord = new NextWord(allWords[i+1], 1); // create a new word that comes after my new first word
      newWord->secondWordList.push_back(newNextWord); // add it to the new first word's second word list
    }

  }
}

void poem(HashTable<string,FirstWordInfo> & table, string startingWord, int length)
{
  clean(startingWord);
  srand(time(NULL));
  while(length > 0)
  {
    FirstWordInfo *foundWord = table.find(startingWord);
    cout << startingWord << " ";
    int random = rand() % foundWord->getCount();

    int followingWordsCount = 0;

    for (int i = 0; i < foundWord->secondWordList.size(); i++)
    {
      followingWordsCount += foundWord->secondWordList[i]->count;
      if (followingWordsCount > random)
      {
        startingWord = foundWord->secondWordList[i]->word;
        break;
      }
    }

    length--;
  }

  cout << endl << endl;
}

int main() {

  HashTable<string,FirstWordInfo> green;
  fillHash(green,readPoem("../green.txt"));

  HashTable<string,FirstWordInfo> clown;
  fillHash(clown,readPoem("../clown.txt"));

  HashTable<string,FirstWordInfo> inch;
  fillHash(inch, readPoem("../inch.txt"));

  HashTable<string,FirstWordInfo> Poe;
  fillHash(Poe,readPoem("../Poe.txt"));

  HashTable<string,FirstWordInfo> Seuss;
  fillHash(Seuss,readPoem("../Seuss.txt"));

  cout<< endl;

  // test code
  cout << "GREEN POEM" << endl << endl;
  poem(green,"sam",20);
  cout << "GREEN TABLE" << endl << endl << green.toString() << endl;

  cout << "CLOWN POEM" << endl << endl;
  poem(clown,"go",20);
  cout << "CLOWN TABLE" << endl << endl << clown.toString() << endl;

  cout << "INCH POEM" << endl << endl;
  poem(inch,"computer",50);

  cout << "POE POEM" << endl << endl;
  poem(Poe,"nevermore",50);

  cout << "SEUSS POEM" << endl << endl;
  poem(Seuss,"mordecai",50);

}
