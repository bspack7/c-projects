//
// Created by Brett Spackman on 3/15/18.
//

#ifndef CS3HW4_NEXTWORD_H
#define CS3HW4_NEXTWORD_H

#include <algorithm>
#include <string>
#include <sstream>
#include "HashTable.h"

using namespace std;

class NextWord {
public:
  std::string word;
  int count;
  std::string toString();
  NextWord(std::string s, int c);
  void increment() { count++; };
};

class FirstWordInfo
{
  std::string word;
  int count;
public:
  std::vector<NextWord*> secondWordList; // vector of my next words
  std::string toString();
  FirstWordInfo(std::string s, int c);
  // void updateSecondWord(std::string w);
  void incrementCount(){ count++;}
  int getCount() {return count;};
};

string FirstWordInfo::toString()
{
  // prints the word and count in the hash and accesses next word's to string function to print out the words that follow and their count

  std::stringstream sstream;
  sstream << word << " " << count << " ";
  for (int i = 0; i < secondWordList.size() ; i++)
  {
    sstream << secondWordList[i]->toString();
  }

  return sstream.str();
}

FirstWordInfo::FirstWordInfo(std::string s, int c)
{
  word = s;
  count = c;
}

//void FirstWordInfo::updateSecondWord(std::string w)
//{
//
//}

string NextWord::toString()
{
  std::stringstream sstream;
  sstream << word << " " << count << " ";

  return sstream.str();
}

NextWord::NextWord(std::string s, int c)
{
  word = s;
  count = c;
}

#endif //CS3HW4_NEXTWORD_H
