#include <iostream>
#include <fstream>
#include <iomanip>
#include <utility>
#include <vector>

#include "Record.hpp"
#include "TestCases-3.hpp"

std::vector<unsigned int> readNumbersFromFile (std::string fileName)
{
  unsigned int currentNumber, firstNumber;
  std::vector<unsigned int> txtVector;
  std::ifstream myfile;
  myfile.open (fileName);
  myfile>>firstNumber;
  int i = 0;
  while (i < firstNumber)
  {
    myfile>>currentNumber;
    txtVector.push_back (currentNumber);
    ++i;
  }
  return txtVector;
};

std::vector<Record*> createVectorOfRecordsFromVectorOfNumbers (std::vector<unsigned int> inputNumbers)
{
  std::vector<Record*> outputPointers;

  for (int i = 0; i < inputNumbers.size(); i++)
  {
    int start = 0;
    int end = outputPointers.size() - 1;

    bool found = false;
    while (start <= end) {
      int middle = ((end + start) / 2);
      if (outputPointers[middle]->getNumber() == inputNumbers[i])
      {
        // it is the middle number
        outputPointers[middle]->incrementFrequency();
        found = true;
        break;
      }

      else if (inputNumbers[i] > outputPointers[middle]->getNumber())
      {
        // it lays on the right side of the half
        start = middle + 1;
      }

      else if (inputNumbers[i] < outputPointers[middle]->getNumber())
      {
        // it lays on the left side of the half
        end = middle - 1;
      }
    }

    if (!found)
    {
      Record*tempRecord = new Record(inputNumbers[i]);
      outputPointers.insert (outputPointers.begin() + start, tempRecord);
    }
  }

  return outputPointers;
}

void sortByFrequency(std::vector<Record*> & outputPointers)
{
  for (int start = 0; start < outputPointers.size() - 1; start++)
  {
    int minPos = start;
    for (int i = start + 1; i < outputPointers.size(); i++)
    {
      if (outputPointers[i]->getFrequency() > outputPointers[minPos]->getFrequency())
      {
        minPos = i;
      }
    }
    std::swap(outputPointers[start], outputPointers[minPos]);
  }
}

void reportFrequencies(std::vector<Record*> & outputPointers)
        {
          int sameFreq = 1;
          int currentTotal = 0;
          int runningTotal = 0;
          int oldFreq = outputPointers[0]->getFrequency();
              for (int i = 1; i < outputPointers.size(); i++)
                {
                  int tempFreq = outputPointers[i]->getFrequency();
                  if (tempFreq == oldFreq)
                  {
                    sameFreq++;
                  }
                  else
                  {
                    currentTotal = sameFreq * outputPointers[i-1]->getFrequency();
                    runningTotal += currentTotal;
                    std::cout << sameFreq << " Numbers with a count of " << outputPointers[i-1]->getFrequency() << std::endl;
                    sameFreq = 1;
                    oldFreq = tempFreq;
                  }
                }
          std::cout << sameFreq << " Numbers with a count of " << outputPointers[outputPointers.size()-1]->getFrequency() << std::endl;
          runningTotal += sameFreq * outputPointers[outputPointers.size()-1]->getFrequency();
          std::cout << "There were " << runningTotal << " identified during the frequency analysis. \n";
        }

int main()
{
  std::vector<unsigned int> numbers = readNumbersFromFile("input.txt");

  std::vector<Record*> records = createVectorOfRecordsFromVectorOfNumbers(numbers);
  sortByFrequency(records);
  reportFrequencies(records);
  cleanDynamicMemory(records);	// Function stub found in TestCases.cpp

  // Test cases
  executeFreqTest(FreqTestCase1, createVectorOfRecordsFromVectorOfNumbers, "Frequency Test Case 1");
  executeFreqTest(FreqTestCase2, createVectorOfRecordsFromVectorOfNumbers, "Frequency Test Case 2");
  executeSortTest(SortTestCase1, sortByFrequency, "Sort Test Case 1");
  executeSortTest(SortTestCase2, sortByFrequency, "Sort Test Case 2");
  return 0;
}