#include <iostream>
#include "Union.h"

int main() {

  // create vector of people with given size
  Union testUnion(20);

  // a counter for how many days it takes me to connect everyone
  int days = 0;
  srand(time(NULL));
  // while loop that stops when everybody is connected
  while(!testUnion.isUnioned())
  {
  // randomly make a connection within the vector and count it as a day
  int max = 20;
  int min = 0;

  int ranNumOne = rand() % (max);
  int ranNumTwo = rand() % (max);

    if (ranNumOne != ranNumTwo)
    {
      testUnion.unionSet(testUnion.find(ranNumOne), testUnion.find(ranNumTwo));
      days++;
      std::cout << "contents of data structure: " << std::endl;
      testUnion.print();
    }
  }

  std::cout << std::endl << "days used: " << days << std::endl;
  std::cout << "unions: " << testUnion.unionCount << std::endl;
  std::cout << "recursive find calls: " << testUnion.findCount << std::endl;

  return 0;
}