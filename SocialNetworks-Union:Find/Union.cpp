//
// Created by Brett Spackman on 4/5/18.
//

#include "Union.h"
#include <iostream>

void Union::unionSet(int root1, int root2) {
  if (data[root1] < 0 && data[root2] < 0 && root1 != root2)
    {
      unionCount++;
      if (data[root2] < data[root1])
      {
        data[root1] = root2;
      }
      else {
        if (data[root1] == data[root2])
        {
          data[root1]--;
        }
        // weight  stored as a negative,  this increases height.
        data[root2] = root1;
      }
    }
}

int Union::find(int x)
{
  findCount++;
  if (data[x] < 0)
  {
    return x;
  }
  else
  {
    return data[x] = find(data[x]);
  }
}

bool Union::isUnioned()
{
  int negs = 0;
  for (int i = 0; i < data.size(); ++i) {
    if (data[i] < 0)
    {
      negs++;
    }
  }
  if (negs > 1)
  {
    return false;
  }
  else
  {
    return true;
  }
}

void Union::print()
{
  for (int i = 0; i < data.size(); ++i)
  {
    std::cout << i << "[" << data[i] << "]" << " ";
  }
  std::cout << std::endl;
}

