//
// Created by Brett Spackman on 4/5/18.
//

#ifndef CS3HW6_UNION_H
#define CS3HW6_UNION_H


#include <vector>

class Union {
public:
  Union(int size){
    this->size = size;
    for (int i = 0; i < size; i++)
    {
      data.push_back(-1);
    }
  };
  void unionSet(int root1, int root2);
  int find(int x);
  void print();
  bool isUnioned();
  int unionCount = 0;
  int findCount = 0;

private:
  std::vector<int> data;
  int size;
};


#endif //CS3HW6_UNION_H
