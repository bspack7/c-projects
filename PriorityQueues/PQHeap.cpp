#include "PQHeap.h"

//Create a heap with maximum size "physicalSize" 
PQHeap::PQHeap(std::string name,int physicalSize ) 
{  
	this->name = name;
	size=0;
	heap = new ItemType[physicalSize];
	this->physicalSize = physicalSize;
}



// insert element and re-heapify
void PQHeap::insert(ItemType &newVal)
{  
	int curr = size;
	assert(size < physicalSize);
	int parent = (curr-1)/KIDS; // round down
	while( curr>0 && newVal.priority >heap[parent].priority ) {
		heap[curr] = heap[parent];
		curr = parent;
		parent = (curr-1)/KIDS;
	}
	heap[curr] = newVal;
	size++;
}


// Delete maximum value, re-heapify, and return max element
ItemType PQHeap::deleteMax( )
{  	
	if (size==0)
		return ItemType("error", -1);
	ItemType toReturn = heap[0];
	size--;
	ItemType currVal = heap[size]; // last element of heap
	int curr = 0;
	int first = 1;
	int second = first+1;
	int third = first + 2;
	int fourth = first + 3;
	while( ( first < size && currVal.priority < heap[first].priority) || (second < size && currVal.priority < heap[second].priority)
				 || (third < size && currVal.priority < heap[third].priority ) || (fourth < size && currVal.priority < heap[fourth].priority))
	{
		int bestChild = first;

		if( second < size && heap[second].priority > heap[first].priority )
		{
			bestChild = second;
		}

		if (third < size && heap[third].priority > heap[first].priority && heap[third].priority > heap[second].priority )
		{
			bestChild = third;
		}

		if (fourth < size && heap[fourth].priority > heap[first].priority && heap[fourth].priority > heap[second].priority
						&& heap[fourth].priority > heap[third].priority)
		{
			bestChild = fourth;
		}

		heap[curr] = heap[bestChild]; // set new root to the larger of left and right
		curr = bestChild;
		first = KIDS*bestChild+1;
		second = first+1;
		third = first + 2;
		fourth = first + 3;
	}

	heap[curr] = currVal;
	return toReturn;

}


// merge second into current  heap
void PQHeap::merge(PQ * second)
{  
	for (int i=0; i < second->getSize(); i++)
		insert(dynamic_cast<PQHeap*>(second)->heap[i]);
	second->makeEmpty();
}

// Convert first "size: elements of PQ to a string.
std::string PQHeap::toString(int maxSize) const
{   const int PERLINE = 5;
	std::stringstream out;
	out <<  name << "  current size=" << size;
	if (maxSize > size)
		maxSize = size;

	for (int i=0; i <maxSize; i ++)
	{  
		if (i%PERLINE==0) out << std::endl;
		out << heap[i].toString() << "\t";
	}
	out << std::endl;
	return out.str();
}