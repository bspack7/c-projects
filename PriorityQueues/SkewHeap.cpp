//
// Created by Brett Spackman on 3/22/18.
//

#include "SkewHeap.h"

void SkewHeap::insert(Node * node)
{
  root = SkewHeapMerge(root, node);
}

void SkewHeap::insert(ItemType item)
{
  Node * tempNode = new Node(item, NULL, NULL);
  root = SkewHeapMerge(root, tempNode);
}

Node * SkewHeap::SkewHeapMerge(Node *H1, Node *H2)
{
  Node * smallestNode;
  if (H1 == NULL) return H2;
  if (H2 == NULL) return H1;
  if (H1->element.priority < H2->element.priority)
  {
    H1->right = SkewHeapMerge(H1->right, H2);
    smallestNode = H1;
  }
  else
  {
    H2->right = SkewHeapMerge(H2->right, H1);
    smallestNode = H2;
  }
  swapChildren(smallestNode);
  return smallestNode;
}

void SkewHeap::swapChildren(Node *swapHead)
{
  std::swap(swapHead->left, swapHead->right);
}

Node * SkewHeap::deleteMin()
{
  Node * temp = root;
  root = SkewHeapMerge(root->right, root->left);

  // std::cout << temp->element.word << " " << temp->element.priority << std::endl;

  return temp;
}

void SkewHeap::printTree (Node * start, std::string indent)
{
  if (start == NULL)
  {
    return;
  }

  printTree(start->right,indent + " ");
  std::cout << indent << start->element.priority << " " << start->element.word  << std::endl;
  printTree(start->left, indent + " ");
}

