#include <iostream>
#include "Queue.h"
#include "Board.h"

void play(std::string fn) {
//create board from text file (ex board1.txt)
  Board board1;
  board1.inputBoard(fn);
  Board winningBoard;
  winningBoard.makeBoard(0);

//put board in queue
  Queue firstQueue;
  GameState initialState(board1, -1, 0, "");
  firstQueue.enqueue(initialState);
  std::cout << initialState.toString() << std::endl;


  int count = 0;
  bool won = false;

  while (!won) {
    GameState pull = firstQueue.dequeue();

    for (int i = 0; i < 12; i++) {
      GameState temp = pull;
      std::string catchit = pull.allMoves + temp.current.move(i);
      GameState newState(temp.current, pull.stateID, count, catchit);
      firstQueue.enqueue(newState);
      std::cout << newState.toString() << std::endl;
      count++;
      if (newState.current == winningBoard) {
        won = true;

        std::cout << newState.toString() << std::endl;
        std::cout << "Congrats you won!" << std::endl << std::endl;
        break;
      }
    }
  }
};

void playJumble () {
  //create board from text file (ex board1.txt)
  Board board1;
  board1.makeBoard(3);
  Board winningBoard;
  winningBoard.makeBoard(0);

//put board in queue
  Queue firstQueue;
  GameState initialState(board1, -1, 0, "");
  firstQueue.enqueue(initialState);
  std::cout << initialState.toString() << std::endl;


  int count = 0;
  bool won = false;

  while (!won) {
    GameState pull = firstQueue.dequeue();

    for (int i = 0; i < 12; i++) {
      GameState temp = pull;
      std::string catchit = pull.allMoves + temp.current.move(i);
      GameState newState(temp.current, pull.stateID, count, catchit);
      firstQueue.enqueue(newState);
      std::cout << newState.toString() << std::endl;
      count++;
      if (newState.current == winningBoard) {
        won = true;

        std::cout << newState.toString() << std::endl;
        std::cout << "Congrats you won!" << std::endl << std::endl;
        break;
      }
    }
  }
};

int main() {
  std::cout << "Input 1:" << std::endl;
  play("board1.txt");
  std::cout << "Input 2:" << std::endl;
  play("board2.txt");
  std::cout << "Input 3:" << std::endl;
  play("board3.txt");
  std::cout << "Jumbled Board" << std::endl;
  playJumble();

  return 0;
}