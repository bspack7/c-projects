It takes far too many moves to find the correct solution. If you look at the history of what got me the correct answers,
the moves really weren't that complicated. I would try different ways of traversing through the tree to try and increase
my efficiency.