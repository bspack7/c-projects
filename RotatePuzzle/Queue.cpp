#include "Queue.h"

#include <iostream>
#include <cstdlib>

Queue::Queue()
{
  front = nullptr;
  tail = nullptr;
}

Queue::~Queue()
{
  Node * trash = front;
  while (trash != nullptr)
  {
    front = front->next;
    trash->next = nullptr;
    delete trash;
    trash = front;
  }
}

void Queue::enqueue(GameState currentBoard)
{
  Node * temp = new Node(currentBoard);
  if (front == nullptr)
  {
    front = temp;
    tail = temp;
  }

  else
  {
    tail -> next = temp;
    tail = temp;
  }
}

GameState Queue::dequeue()
{
  GameState top = front->currentState;
  front = front->next;
  return top;
}
