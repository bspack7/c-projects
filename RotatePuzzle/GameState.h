#ifndef GAMESTATE_H
#define GAMESTATE_H
#include "Board.h"

class GameState
{
public:
  Board current;   // Contents of current board
  int stateID;     // State ID of the current board
  int prevID;      // State ID of the previous board
  std::string allMoves; // What are all the moves you made?
  std::string toString() // Convert GameState to printable form
  {
    std::stringstream ss;
    ss << "State " << stateID << " From State" << prevID << " History : " << allMoves << std::endl;
    ss << current.toString();
    return ss.str();
  }
  GameState(Board curr, int prev, int currID, std::string all)
  {      current = curr;
    stateID = currID;
    prevID = prev;
    allMoves = all;
  }
  GameState()
  { current.makeBoard();
  }
};

#endif