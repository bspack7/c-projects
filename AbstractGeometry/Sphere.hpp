#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "Geometry.hpp"
const double PI  =3.141592653589793238463;

class Sphere:public Geometry {
private:
  int m_radius;
  std::string m_name;
  std::string m_type;

public:
  double computeVolume() override {return 4.0/3.0*PI*(m_radius*m_radius*m_radius);}

  double computeSurface() override {return 4*PI*m_radius*m_radius;}

  Sphere (std::string name, int r) : Geometry(name, "Sphere")
  {
    m_radius = r;
    m_name = name;
    m_type = "Sphere";
  }

  std::string getName() const override
  {
    return m_name;
  }

  std::string getType() const override
  {
    return m_type;
  }
};

#endif