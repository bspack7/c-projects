#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include <iostream>
#include <string>

class Geometry {

private:
  std::string m_name;
  std::string m_type;

public:
  Geometry (std::string name, std::string type)
  {
    m_name = name;
    m_type = type;
  }

  virtual std::string getName() const = 0;
  virtual std::string getType() const = 0;

  virtual double computeVolume() = 0;
  virtual double computeSurface() = 0;

};

#endif

// Must be an abstract class.
// An std::string data member, m_name, that stores a description of the object.
// An std::string data member, m_type, that stores the type (e.g., Box or Sphere) of the object.
// A method named getName that returns the name of the object.
// A method named getType that returns the type of the object.
// A method named computeVolume that computes the volume of the object.
// A method named computeSurface that computes the surface area of the object.