#ifndef BOX_HPP
#define BOX_HPP

#include "Geometry.hpp"

class Box:public Geometry {
private:
  int m_width;
  int m_height;
  int m_length;
  std::string m_name;
  std::string m_type;

public:
  double computeVolume() override { return m_width * m_height * m_length; }

  double computeSurface() override {return 2*m_width*m_height + 2*m_height*m_length  + 2*m_width*m_length;}

  Box (std::string name, int l, int w, int h) : Geometry(name, "Box")
  {
    m_length = l;
    m_width = w;
    m_height = h;
    m_name = name;
    m_type = "Box";
  }

  std::string getName() const override
  {
    return m_name;
  }

  std::string getType() const override
  {
    return m_type;
  }

};

#endif