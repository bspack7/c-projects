#ifndef _RATIONAL_HPP_
#define _RATIONAL_HPP_

#include <fstream>

class TestCases;

class Rational
{
public:

	const char* getName() { return m_name; }
	void report(std::ostream& strm);

	friend TestCases;
	Rational(); // default constructor
	Rational(int, int, char*); // overloaded constructor
	~Rational; // deconstructor that cleans allocated memory
	Rational(const Rational& obj); //copy constuctor, this is wrong, check the examples in "NumberArray"

  Rational& operator=(const Rational& rhs);

  Rational add(const Rational& x);
  Rational subtract(const Rational& x);
  Rational multiply(const Rational& x);
  Rational divide(const Rational& x);
  Rational reciprocal();
  Rational power(int x);


private:
	int m_numerator;
	int	m_denominator;
	char* m_name;

	void reduce();
	int gcd(int, int);
	void setDenominator(int denominator);
};

#endif
