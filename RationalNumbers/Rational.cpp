#include "Rational.hpp"
#include <cstring>
#include <cmath>
#include <math.h>

Rational::Rational()
{
  m_numerator = 0;
  m_denominator = 1;
  m_name = new char[4];
  name[1] = 'n';
  name[2] = '/';
  name[3] = 'a';
  name[4] = null;
}

Rational::Rational(int numerator, int denominator, char *name)
{
  m_numerator = numerator;
  m_denominator = denominator;
  m_name = *name;
}

Rational::~Rational()
{
  delete[] m_name;
}

Rational::Rational(const Rational& newValue)
{
  m_numerator = newValue.m_numerator;
  m_denominator = newValue.m_denominator;
  m_name = new char[strlen(newValue.m_name) + 1];
  strcpy (m_name, newValue.m_name);
}

Rational& Rational::operator=(const Rational& rhs)
{
  m_numerator = rhs.m_numerator;
  m_denominator = rhs.m_denominator;
  m_name = new char[strlen(rhs.m_name) + 1];
  strcpy (m_name, rhs.m_name);
  return *this;
}

Rational Rational::add(const Rational& x)
{
  Rational answer;
  if (this->m_denominator == x.m_denominator)
  {
    answer.m_denominator = x.m_denominator;
    answer.m_numerator = this->m_numerator + x.m_numerator;
    answer.reduce();
    return answer;
  }
  else
  {
    answer.m_denominator = this->m_denominator * x.m_denominator;
    answer.m_numerator = (this->m_numerator * x.m_denominator) + (x.m_numerator * this->m_denominator);
    answer.reduce();
    return answer;
  }
}

Rational Rational::subtract(const Rational& x) {
  Rational answer;
  if (this->m_denominator == x.m_denominator)
  {
    answer.m_denominator = x.m_denominator;
    answer.m_numerator = this->m_numerator - x.m_numerator;
    answer.reduce();
    return answer;
  }
  else
  {
    answer.m_denominator = this->m_denominator * x.m_denominator;
    answer.m_numerator = (this->m_numerator * x.m_denominator) - (x.m_numerator * this->m_denominator);
    answer.reduce();
    return answer;
  }
}

Rational Rational::multiply(const Rational& x)
{
  Rational answer;
  answer.m_numerator = this->m_numerator * x.m_numerator;
  answer.m_denominator = this->m_denominator * x.m_denominator;
  answer.reduce();
  return answer;
}

Rational Rational::divide(const Rational& x)
{
  Rational answer;
  answer.m_numerator = this->m_numerator * x.m_denominator;
  answer.m_denominator = this->m_denominator * x.m_numerator;
  answer.reduce();
  return answer;
}

Rational Rational::reciprocal()
{
  Rational rec;
  int n = m_numerator,
      d = m_denominator;
  rec.m_denominator = d;
  rec.m_numerator = n;
  rec.reduce();
  return rec;
}

Rational Rational::power(int x)
{
  int n = m_numerator,
      d = m_denominator;
  n = n ^ x;
  d = d ^ x;
  Rational p;
  p.m_numerator = n;
  p.m_denominator =d;
  return power;
}

void Rational::report(std::ostream& strm)
{
	strm << m_numerator << " / " << m_denominator;
}

//-------------------------------------------------------------------
//
// Sets denominator to an integer value. If the denominator is negative
// then it is changed to a positive value and the numerator of the
// Rational is changed to a negative to avoid negatives in the denominator.
// It also calls the reduce function incase a simplification can be made.
//
//-------------------------------------------------------------------
void Rational::setDenominator(int b)
{
	if (b == 0)	// Prevents 0 denominators
	{
		//
		// TODO: We can eventually do something better, but doing this for now
		m_denominator = 1;
	}
	else if (b < 0)  // Corrects negative denominators
	{
		m_numerator = -1 * m_numerator;
		m_denominator = -1 * b;
		reduce();
	}
	else
	{
		m_denominator = b;
		reduce();
	}
}

//-------------------------------------------------------------------
//
// Reduces Rationals to their simplified form by dividing the num and den by
// the greatest common divisor
//
//-------------------------------------------------------------------
void Rational::reduce()
{
	int commonDivisor = gcd(abs(m_numerator), m_denominator);
	m_numerator = (m_numerator / commonDivisor);
	m_denominator = (m_denominator / commonDivisor);
}

//-------------------------------------------------------------------
//
// Finds the gcd of two integers
//
//-------------------------------------------------------------------
int Rational::gcd(int numerator, int denominator)
{
	if (denominator == 0)
	{
		return numerator;
	}

	return gcd(denominator, numerator % denominator);
}
