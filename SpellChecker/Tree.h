#ifndef TREE_HPP
#define TREE_HPP

#include <iostream>
#include <string>
#include <memory>

template <typename T>

class Tree {
public:
  Tree() :
          root(nullptr) {}

  bool insert(T word);

  void remove(T word);

  bool search(T word);

  void display();

  unsigned int numberNodes();

  unsigned int numberLeafNodes();

  unsigned int height();

private:
  class Node
  {
  public:
    Node(T word): data(word), left(nullptr), right(nullptr) {}

    T data;
    std::shared_ptr<Node> left;
    std::shared_ptr<Node> right;
  };

  std::shared_ptr<Node> root;

  void insert(std::shared_ptr<Node>& ptrNode, std::shared_ptr<Node>& newNode);
  void remove(std::shared_ptr<Node>& ptrNode, T word);
  void remove(std::shared_ptr<Node>& ptrNode);
  bool search(std::shared_ptr<Node>& ptrNode, T word);

  void display(std::shared_ptr<Node>& ptrNode);
  unsigned int numberNodes(std::shared_ptr<Node>& ptrNode);
  unsigned int numberLeafNodes(std::shared_ptr<Node>& ptrNode);
  unsigned int height(std::shared_ptr<Node>& ptrNode);
};

template <typename T>
bool Tree<T>::insert(T word){

  if (search(root, word))
  {
    return false;
  }
  else
  {
    std::shared_ptr<Node> ptrNew = std::make_shared<Node>(word);
    insert(root, ptrNew);
    return true;
  }
}

template <typename T>
void Tree<T>::insert(std::shared_ptr<Node>& ptrNode, std::shared_ptr<Node>& ptrNew){
  if (ptrNode== nullptr)
  {
    ptrNode = ptrNew;
  }
  else if (ptrNew->data < ptrNode->data)
  {
    insert(ptrNew->left,ptrNew);
  }
  else
  {
    insert(ptrNew->right,ptrNew);
  }
}

template <typename T>
void Tree<T>::remove(T word)
{
  remove(root, word);
}

template <typename T>
void Tree<T>::remove(std::shared_ptr<Node>& ptrNode, T word) {
  if (ptrNode == NULL) return;
  if (word < ptrNode->data)
  {
    remove(ptrNode->left, word);
  }
  else if (word > ptrNode->data)
  {
    remove(ptrNode->left, word);
  }
  else
  {
    remove(ptrNode);
  }
}

template <typename T>
void Tree<T>::remove(std::shared_ptr<Node>& ptrNode) {

  std::shared_ptr<Node>& deleteNode= ptrNode;

  if (ptrNode->right == NULL)
  {
    ptrNode = ptrNode->left;
  }
  else if (ptrNode->left == NULL)
  {
    ptrNode = ptrNode->right;
  }
  else
  {
    std::shared_ptr<Node>& attachpoint = ptrNode->right;
    while (attachpoint->left != NULL)
    {
      attachpoint = attachpoint->left;
      attachpoint->left = ptrNode->left;
      ptrNode = ptrNode->right;
    }
  }
}

template <typename T>
bool Tree<T>::search(T word) {
  return search(root, word);
}

template <typename T>
bool Tree<T>::search(std::shared_ptr<Node>& ptrNode, T word){
  std::shared_ptr<Node> ptr = this->root;
  while (ptr != nullptr)
  {
    if (ptr->data == word)
    {
      return true;
    }
    else if (ptr->data < word)
    {
      ptr = ptr->right;
    }
    else
    {
      ptr = ptr->left;
    }
  }
  return false;
};

template <typename T>
void Tree<T>::display(){
  display(root);
}

template <typename T>
void Tree<T>::display(std::shared_ptr<Node>& ptrNode){
  if (ptrNode == nullptr) return;

  display(ptrNode->left);
  std::cout << ptrNode->data << std::endl;
  display(ptrNode->right);
}

template <typename T>
unsigned int Tree<T>::numberNodes(){
    return numberNodes(root);
}

template <typename T>
unsigned int Tree<T>::numberNodes(std::shared_ptr<Node>& ptrNode){
  if (ptrNode == nullptr)
  {
    return 0;
  }
  return numberNodes(ptrNode->left) + 1 + numberNodes(ptrNode->right);
}

template <typename T>
unsigned int Tree<T>::numberLeafNodes(){
    return numberLeafNodes(root);
}

template <typename T>
unsigned int Tree<T>::numberLeafNodes(std::shared_ptr<Node>& ptrNode){
  if (ptrNode == nullptr)
  {
    return 0;
  }

  else if (ptrNode->left == nullptr && ptrNode->right == nullptr)
  {
    return 1;
  }

  else
  {
    return numberLeafNodes(ptrNode->left) + numberLeafNodes(ptrNode->right);
  }
}

template <typename T>
unsigned int Tree<T>::height(){
  return height(root);
}

template <typename T>
unsigned int Tree<T>::height(std::shared_ptr<Node>& ptrNode){
  if (ptrNode == nullptr)
  {
    return 0;
  }

  unsigned leftTree = height(ptrNode->left);
  unsigned rightTree = height(ptrNode->right);

  if (leftTree > rightTree)
  {
    return leftTree + 1;
  }

  else
  {
    return rightTree + 1;
  }
}

#endif

/*
bool insert(T value)
inserts a value into the tree, does not allow duplicates.  If the value is already in the tree do not add the duplicate and return false, true otherwise.

void remove(T value)
deletes a value from the tree.

bool search(T value)
tests to see if a value exists in the tree.

void display()
Performs an in-order traversal of the tree, displaying the value at each node.

unsigned int numberNodes()
returns a count of all nodes in the tree.

unsigned int numberLeafNodes()
returns a count of all the leaf nodes in the tree.

unsigned int height()
returns the height of the tree.
 */