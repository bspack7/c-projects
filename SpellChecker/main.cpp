#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>
#include "Tree.h"

void testTree()
{
  Tree<std::string> tree;
  //
  // Add a bunch of values to the tree
  tree.insert("Olga");
  tree.insert("Tomeka");
  tree.insert("Benjamin");
  tree.insert("Ulysses");
  tree.insert("Tanesha");
  tree.insert("Judie");
  tree.insert("Tisa");
  tree.insert("Santiago");
  tree.insert("Chia");
  tree.insert("Arden");

  //
  // Make sure it displays in sorted order
  tree.display();

  //
  // Try to add a duplicate
  std::cout << std::endl << "---- adding a duplicate ----" << std::endl;
  if (tree.insert("Tomeka"))
  {
    std::cout << "oops, shouldn't have returned true from the insert" << std::endl;
  }
  tree.display();

  //
  // Remove an existing value from the tree
  std::cout << std::endl << "---- removing an existing value ----" << std::endl;
  tree.remove("Olga");
  tree.display();

  //
  // Remove a value that was never in the tree, hope it doesn't crash!
  tree.remove("Karl");

  //
  // Check the tree stats
  std::cout << std::endl << "---- checking the tree stats ----" << std::endl;
  std::cout << "Expecting 9 nodes, found " << tree.numberNodes() << std::endl;
  std::cout << "Expecting 4 leaf nodes, found " << tree.numberLeafNodes() << std::endl;
  std::cout << "Expecting height of 6, found " << tree.height() << std::endl;
}

void readLetter(Tree <std::string> Tree)
{
  std::ifstream letter("Letter.txt");
  std::string currentLine;
  std::string temp;
  std::vector <std::string> wordsVector;
  if (letter.is_open())
  {
    std::cout << std::endl << "--Misspelt Words---" << std::endl << std::endl;
    while (!letter.eof())
    {
      letter >> currentLine;
      temp = currentLine;
      for (int c = 0; c < currentLine.length(); c++)
      {
        if (currentLine[c] == ',' || currentLine[c] == '"' || currentLine[c] == '.' || currentLine[c] == ':' || currentLine[c] == '!' || currentLine[c] == '?')
        {
          currentLine.erase(c, 1);
        }
        temp[c] = (tolower(currentLine[c]));
      }
      if (!Tree.search(temp))
      {
        std::cout << currentLine << std::endl;
      }
      letter.close();
    }
  }

  else
  {
    std::cout << "unnable to open file" << std::endl;
  }
};

int main() {

  std::ifstream file;
  file.open("Dictionary.txt");
  std::vector<std::string> words;
  if (file.is_open())
  {
    while (!file.eof())
    {
      std::string currentWord;
      file >> currentWord;
      if (currentWord != " ")
      {
        words.push_back(currentWord);
      }
    }
  }
  long seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::shuffle (words.begin(), words.end(), std::default_random_engine(seed));

  Tree<std::string> T;
  for (std::string s: words)
  {
    T.insert(s);
  }

  testTree();

  readLetter(T);

  return 0;
}