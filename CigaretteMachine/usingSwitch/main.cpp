/************************
* A#: A01912594
* Course: CS1400
* CS1400 Section: 2
* CS1405 Section: 16
* HW#: 6
***********************/

#include <iostream>
#include <iomanip>

using namespace std;

int main() {

  char menuOption;
  cout << "s - report the machine status\n" << "d - drop in a quarter\n" << "1 - pull the 1st knob\n" << "2 - pull the 2nd knob\n" << "3 - pull the 3rd knob\n" << "4 - pull the 4th knob\n" << "r - restock the machine\n" << "q - quit\n";


  bool cont = true;

  int luckyStrike = 5, camel = 7, gauloises = 1, pallMall = 6, quarters = 0;
  float total = 24.50;

  while (cont != false) {
    cin >> menuOption;

    switch (menuOption) {

      case 's':
        cout << "1: " << luckyStrike << " packs of Lucky Strikes\n" << "2: " << camel << " packs of Camels\n" << "3: " << gauloises << " packs of Gauloises\n" << "4: " << pallMall << " packs of Pall Malls\n" << "You have " << quarters << " quarters to spend\n" << "There is $" << fixed << setprecision(2) << total << " in the machine\n";
        break;

      case 'd':
        quarters = quarters + 1;
        cout << "ching\n";
        break;

      case '1':
        if (quarters >= 3 && luckyStrike > 0) {
          total = total + (quarters * .25);
          quarters = quarters - 3;
          luckyStrike = luckyStrike - 1;
          cout << "A pack of Lucky Strikes slides into view. It's your lucky day!\n";
        } else if (luckyStrike == 0) {
          cout << "You hear mechanical clanking, but no cigarettes appear.\n";
        } else {
          quarters = 0;
          cout << "(nothing happens)\n";
        }
        break;

      case '2':
        if (quarters >= 3 && camel > 0) {
          total = total + (quarters * .25);
          quarters = quarters - 3;
          camel = camel - 1;
          cout << "A pack of Camels slides into view. Watch out, they spit!\n";
        } else if (camel == 0) {
          cout << "You hear mechanical clanking, but no cigarettes appear.\n";
        } else {
          quarters = 0;
          cout << "(nothing happens)\n";
        }
        break;

      case '3':
        if (quarters >= 3 && gauloises > 0) {
          total = total + (quarters * .25);
          quarters = quarters - 3;
          gauloises = gauloises - 1;
          cout << "A pack of Gauloises slides into view. Gauloises? Really?\n";
        } else if (gauloises == 0) {
          cout << "You hear mechanical clanking, but no cigarettes appear.\n";
        } else {
          quarters = 0;
          cout << "(nothing happens)\n";
        }
        break;

      case '4':
        if (quarters >= 3 && pallMall > 0) {
          total = total + (quarters * .25);
          quarters = quarters - 3;
          pallMall = pallMall - 1;
          cout << "A pack of Pall Malls slides into view. They rhyme!\n";
        } else if (pallMall == 0) {
          cout << "You hear mechanical clanking, but no cigarettes appear.\n";
        } else {
          quarters = 0;
          cout << "(nothing happens)\n";
        }
        break;

      case 'r':
        cout << "A grouchy-looking attendant shows up and restocks the machine.\n";
        luckyStrike = 10;
        camel = 10;
        gauloises = 10;
        pallMall = 10;
        total = 0;
        break;

      case 'q':
        cont = false;
        cout << "So long!\n";
        break;

      default:
        cout << "I do not understand.\n";
      }
    }
    return 0;
  }