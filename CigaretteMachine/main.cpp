#include <iostream>
#include <iomanip>

using namespace std;

int main() {
  char menuOption;

  cout << "s - report the machine status\n"
          "d - drop in a quarter\n"
          "1 - pull the 1st knob\n"
          "2 - pull the 2nd knob\n"
          "3 - pull the 3rd knob\n"
          "4 - pull the 4th knob\n"
          "r - restock the machine\n"
          "q - quit\n";
  cin >> menuOption;

  bool cont = true;

  while (cont != false) {


    int luckyStrike = 5, camel = 7, gauloises = 1, pallMall = 6, quarters = 0;
    float total = 24.50;

    if (menuOption == 's') {

      cout << "1: " << luckyStrike << " packs of Lucky Strikes\n"
              "2: " << camel << " packs of Camels\n"
                   "3: " << gauloises << " packs of Gauloises\n"
                   "4: " << pallMall << " packs of Pall Malls\n"
                   "You have " << quarters << " quarters to spend\n"
                   "There is $" << fixed << setprecision(2) << total << " in the machine\n";
    }

    if (menuOption == 'd') {
      quarters = quarters + 1;
      cout << "Ching\n";
    }

    if (quarters >= 3) {

      if (menuOption == 1) {
        luckyStrike = luckyStrike - 1;
        cout << "A pack of Lucky Strikes slides into view. It's your lucky day!\n";
      }

      if (menuOption == 2) {
        camel = camel - 1;
        cout << "A pack of Camels slides into view. Watch out, they spit!\n";
      }

      if (menuOption == 3) {
        int gauloises = gauloises - 1;
        cout << "A pack of Gauloises slides into view. Gauloises? Really?\n";
      }

      if (menuOption == 4) {
        int pallWall = pallWall - 1;
        cout << "A pack of Pall Malls slides into view. They rhyme!\n";
      }
    } else if (quarters < 3) {
      cout << "(nothing happens)\n";
    }
    if (menuOption == 'q'){
      cont = false;
    }
  }
  return 0;
}