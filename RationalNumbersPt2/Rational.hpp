#ifndef _RATIONAL_HPP_
#define _RATIONAL_HPP_

#include <fstream>
#include <cstring>

class TestCases;

class Rational
{
public:

	const char* getName() { return m_name; }
	void report(std::ostream& strm);
	friend TestCases;

	Rational(); // default constructor
	Rational(int, int, const char*); // overloaded constructor
	~Rational(); // deconstructor that cleans allocated memory
	Rational(const Rational& obj); //copy constuctor, this is wrong, check the examples in "NumberArray"

  Rational& operator=(const Rational& rhs);

	Rational operator+(const Rational& rhs);
	Rational operator-(const Rational& rhs);
	Rational operator*(const Rational& rhs);
	Rational operator/(const Rational& rhs);

	Rational operator^(int x);
	Rational operator~();
	Rational operator^=(int x);

	Rational& operator+=(const Rational& rhs);
	Rational& operator-=(const Rational& rhs);
	Rational& operator*=(const Rational& rhs);
	Rational& operator/=(const Rational& rhs);

	bool operator==(const Rational& rhs);
	bool operator<=(const Rational& rhs);
	bool operator>=(const Rational& rhs);
	bool operator!=(const Rational& rhs);

	operator double();

  friend std::ostream& operator<<(std::ostream& os, Rational & a);

private:
	int m_numerator;
	int	m_denominator;
	char* m_name;

	void reduce();
	int gcd(int, int);
	void setDenominator(int denominator);

	Rational add(const Rational& x);
	Rational subtract(const Rational& x);
	Rational multiply(const Rational& x);
	Rational divide(const Rational& x);
	Rational reciprocal();
	Rational power(int x);
};

#endif
