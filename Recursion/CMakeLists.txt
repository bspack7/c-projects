cmake_minimum_required(VERSION 3.8)
project(cs3hw2_1)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES TestTrees.cpp binarytree.h)
add_executable(cs3hw2_1 ${SOURCE_FILES})