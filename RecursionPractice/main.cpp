#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include "TestCases.hpp"

std::string collapseSpaces(std::string s);
std::vector<std::string> split(std::string s);
void quickSort(std::vector<int>& data, int start, int end);

// ------------------------------------------------------------------
//
// Provided code to remove spaces from a string (and capitalize it)
//
// ------------------------------------------------------------------
std::string collapseSpaces(std::string s)
{
	s.erase(std::remove_if(s.begin(), s.end(), isspace), s.end());
	std::transform(s.begin(), s.end(), s.begin(), toupper);	// Capitalize all
	return s;
}

// ------------------------------------------------------------------
//
// Solution string split code
//
// ------------------------------------------------------------------
std::vector<std::string> split(std::string s)
{
	std::istringstream is(s);
	std::vector<std::string> words;

	do
	{
		std::string word;
		is >> word;
		if (word.length() > 0)
		{
			words.push_back(word);
		}
	} while (is);

	return words;
}

// ------------------------------------------------------------------
//
// Provided quicksort partition code
//
// ------------------------------------------------------------------
int partition(std::vector<int>& data, int start, int end)
{
	int middle = (start + end) / 2;
	std::swap(data[start], data[middle]);
	int pivotIndex = start;
	int pivotValue = data[start];
	for (int scan = start + 1; scan <= end; scan++)
	{
		if (data[scan] < pivotValue)
		{
			pivotIndex++;
			std::swap(data[scan], data[pivotIndex]);
		}
	}

	std::swap(data[pivotIndex], data[start]);

	return pivotIndex;
}

// ------------------------------------------------------------------
//
// Provided quicksort code
//
// ------------------------------------------------------------------

// Modify the quicksort algorithm to switch to a selection sort when the number of elements to be sorted is
// less than or equal to 10.  The driver code already includes the quicksort (that we coded during class), you
// need to provide the selection sort and correctly use it in the quicksort.  The driver code also provides a
// timing comparison of our quick/selection sort hybrid compared to the built-in std::sort algorithm.  In Visual
// Studio compile for "Release" to see an accurate timing comparison.  For Linux users, add a "-O3" compiler option,
// this will enable optimization to see an accurate timing comparison.

void selectionSort(std::vector<int>& data, int start, int end)
{
  int s = 0;
  for (int i = start; i < end; i++)
  {
    s = i;
    for (int j = i + 1; j <= end; j++)
    {
      if (data[j] < data[s])
      {
        s = j;
      }
    }
    std::swap(data[i], data[s]);
  }
}

void quickSort(std::vector<int>& data, int start, int end)
{
	if (end - start <= 10)
  {
    selectionSort(data, start, end);
  }
  else
  {
    if (start < end)
    {
      int pivot = partition(data, start, end);
      quickSort(data, start, pivot - 1);
      quickSort(data, pivot + 1, end);
    }
  }
}

bool isPalindrome(std::string word, int start, int end)
{
  if (start >= end)
  {
    return true;
  }

  if (word[start] == word[end])
  {
    return isPalindrome(word, ++start, --end);
  }

  return false;
}

bool isWordSymmetric(const std::vector<std::string>& words, int start, int end)
{
  if (start >= end)
  {
    return true;
  }

  if (words[start] == words[end])
  {
    return isWordSymmetric(words, start+1, end-1);
  }
  return false;
}

long vectorSum(const std::vector<int>& data, unsigned int position)
{
  if (position >= data.size())
  {
    return 0;
  }

  long sum = 0;
  sum = vectorSum(data, position+1);
  return sum + data[position];
}

int vectorMin(const std::vector<int>& data, unsigned int position)
{
  if (position == data.size()-1)
  {
    return data[position];
  }
  int a = vectorMin(data,position + 1);
  int b = data[position];
  if (a < b)
  {
    return a;
  }
  return b;
}

int main()
{
	TestCases::runTestCases();
}
