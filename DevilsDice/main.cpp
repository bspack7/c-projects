/************************
* A#: A01912594
* Course: CS1400
* CS1400 Section: 2
* CS1405 Section: 16
* HW#: 7
***********************/

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

// Function prototypes
void playGame(void);
int roll(bool isPlayerTurn);
char getActionFromUser(void);
void printDisplay(int playerScore, int playerTurnPoints, int devilScore);
int devilTurn(int playerScore, int devilScore);
void updateWinHistory(bool playerWon);

/**
 * Main entry point for the Devil's Dice game
 * @return 0 on successful execution
 */
int main()
{
#ifdef JARVIS
  srand(0);
#else
  srand(time(NULL));
#endif

  playGame();

  return 0;
}

/**
 * Plays a game of Devil's Dice
 */
void playGame(void)
{
  int devilTotalScore = 0;
  int playerTotalScore = 0;
  int playerCurrentTurnPoints = 0;
  bool devilGetsATurn = false;
  bool didPlayerWin = false;
  bool isGameOver = false;

  cout << "---- Welcome to Devil's Dice! ----" << endl;

  while (!isGameOver)
  {
    char action = getActionFromUser();

    // TODO - Handle player actions here, updating provided variables when needed
    if (action == 'r')
    {
      int rollPoints = roll(true);

      if (rollPoints != 1)
      {
        playerCurrentTurnPoints = playerCurrentTurnPoints + rollPoints;
      }

      else if (rollPoints == 1)
      {
        playerCurrentTurnPoints = 0;
        devilGetsATurn = true;
      }
    }

    else if (action == 'h')
    {
      playerTotalScore = playerTotalScore + playerCurrentTurnPoints;
      cout << "You banked " << playerCurrentTurnPoints << " points\n";

      if (playerTotalScore < 100)
      {
        playerCurrentTurnPoints = 0;
        devilGetsATurn = true;
      }

      else if (playerTotalScore >= 100)
      {
        devilGetsATurn = false;
        didPlayerWin = true;
        isGameOver = true;
        cout << "You Win!\n";
      }
    }

    else if (action == 'f')
    {
      isGameOver = true;
      cout << "Game Over!\n";
    }


    if (devilGetsATurn)
    {
      devilTotalScore += devilTurn(playerTotalScore, devilTotalScore);
      devilGetsATurn = false;
    }

    if (!isGameOver)
    {
      printDisplay(playerTotalScore, playerCurrentTurnPoints, devilTotalScore);
    }

    if (devilTotalScore >= 100)
    {
      cout << "Devil Wins!" << endl;
      didPlayerWin = false;
      isGameOver = true;
    }
  }

  updateWinHistory(didPlayerWin);
}

/**
 * Rolls a dice, returning a random value between 1 and 6
 * @param isPlayerTurn True if it is the player rolling, false otherwise
 * @return Random dice roll
 */
int roll(bool isPlayerTurn)
{
  int roll = 0;
  // TODO - Assign random dice roll and print out appropriate message

  roll = (rand() % 6) + 1;

  if (isPlayerTurn)
  {
    cout << "You rolled a " << roll << endl;
  }
  else if (!isPlayerTurn)
  {
    cout << "Devil rolled a " << roll << endl;
  }


  return roll;
}

/**
 * Prompts for and gets an action from the user
 * @return The action selected by the user
 */
char getActionFromUser(void)
{
  char input;

  // TODO - Prompt for and get user input
  cout << "Hold[h], roll[r], or forfeit[f]: ";
  cin >> input;

  return input;
}

/**
 * Prints out the Devil's Dice display
 * @param playerScore Current total player score
 * @param playerTurnPoints Points player has accumulated so far this turn
 * @param devilScore Current total devil's score
 */
void printDisplay(int playerScore, int playerTurnPoints, int devilScore)
{
  // TODO - Print out the nicely formatted display

  cout << right << setw(11) << "Player" << setw(20) << "Devil" << left << setw(5) << "" << endl;
  cout << right << setw(11) << "------" << setw(20) << "-----" << left << setw(5) << "" << endl;

  for (int base = 100; base >= 0; base -= 10)
  {
    if (playerScore >= base && playerScore < base + 10)
    {
      cout << right << setw(3) << playerScore << right << setw(2) << ">";
    }

    else {
      cout << right << setw(3) << "" << right << setw(2) << "";
    }

    cout << left << setw(3) << "--" << left << setw(3) << base;

    if (playerScore + playerTurnPoints >= base && playerScore + playerTurnPoints < base + 10)
    {
      cout << left << setw(2) << "<" << left << setw(3) << playerScore + playerTurnPoints;
    }

    else
    {
      cout << left << setw(2) << "" <<  left << setw(3) << "";
    }

    cout << right << setw(12) << base << right << setw(3) << "--";

    if (devilScore >= base && devilScore < base + 10)
    {
      cout << left << setw(2) << "<" << left << setw(3) << devilScore << endl;
    }

    else
    {
      cout << left << setw(2) << "" << left << setw(3) << "" << endl;
    }
  }
  cout << endl;
}

/**
 * Updates the win/loss history stored in a file
 * @param playerWon True if the player won the game, false otherwise
 */
void updateWinHistory(bool playerWon)
{
  // TODO - Read out win history from file (if exists), update values, and write back to file
  int wins = 0;
  int losses = 0;

  string str;
  ifstream fin;
  fin.open("games.txt");
  fin >> wins;
  fin >> losses;

  if (playerWon) {
    wins++;
  }
  else
  {
    losses++;
  }

  ofstream fout;
  fout.open ("games.txt");
  fout << wins << " ";
  fout << losses << endl;

  cout << "Total Wins: " << wins << endl;
  cout << "Total Losses: " << losses << endl;
}

/**
 * The devil take's his turn
 * @param playerScore Current total player score
 * @param devilScore Current total devil score
 * @return The devil's new total score after his turn has completed
 */
int devilTurn(int playerScore, int devilScore)
{
  int devilTurnScore = 0;
  bool devilTurnOver = false;
  int devilGoal = 21;

  if (playerScore > devilScore)
  {
    devilGoal = 30;
  }

  cout << endl;

  while (!devilTurnOver)
  {
    if ((devilTurnScore < devilGoal) && (devilScore + devilTurnScore < 100))
    {
      int devilRoll = roll(false);

      if (devilRoll == 1)
      {
        devilTurnScore = 0;
        devilTurnOver = true;
      }
      else
      {
        devilTurnScore += devilRoll;
      }
    }
    else
    {
      devilTurnOver = true;
    }
  }

  cout << "Devil got " << devilTurnScore << " points" << endl << endl;
  return devilTurnScore;
}