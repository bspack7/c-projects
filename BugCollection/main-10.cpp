#include <iostream>
#include <vector>

#include "Bug.hpp"
#include "TestCases.hpp"


void printBugs(std::vector<Bug> bugs)
{


  //TODO:write a function that prints all the contents of the

  int bs = bugs.size();
  for (int i = 0; i < bs; i++)
  {
    bugs[i].print();
  }
  //     bug array.  Use Bug::print() for each bug.
}

void sortMass(std::vector<Bug>& bugs)
{
  //TODO:write a function to bubble sort the contents of a

  int bs = bugs.size();
  for (int j = bs - 1; j > 0; j--)
  {
    // For each pair
    for (int i = 0; i < j; i++) {
      if (bugs[i].getMass() < bugs[i + 1].getMass()) {
        std::swap(bugs[i + 1], bugs[i]);
      }
    }
  }
  //     bug array based on the mass
}

int main()
{
  const unsigned int NUMBUGS = 5;

  //TODO: make a vector of NUMBUGS bugs

  std::vector<Bug> bugs(NUMBUGS);

  // another way I found that I could do it
//  std::vector<Bug> bugs;
//  for (int i = 0; i < NUMBUGS; i++)
//  {
//    Bug newBug;
//    bugs.push_back(newBug);
//  }
  //end of TODO

  std::cout << std::endl << "-- Initial Bugs --" << std::endl << std::endl;
  printBugs(bugs);
  executeTest(testCase1, bugs, "Initial Bugs");

  //TODO: add three more bugs to the vector

  for (int i = 0; i < 3; i++)
  {
    Bug newBug;
    bugs.push_back(newBug);
  }
  //end of TODO

  std::cout << std::endl << "-- Three More Bugs --" << std::endl << std::endl;
  printBugs(bugs);
  executeTest(testCase2, bugs, "Three More Bugs");

  sortMass(bugs);

  std::cout << std::endl << "-- Sorted By Mass --" << std::endl << std::endl;
  printBugs(bugs);
  executeTest(testCase3, bugs, "Sorted By Mass");

  //TODO: get rid of the 5 bugs with the greatest mass
  bugs.erase (bugs.begin(), bugs.begin() + NUMBUGS);
  //end of TODO

  std::cout << std::endl << "-- Removed Five Biggest --" << std::endl << std::endl;
  printBugs(bugs);
  executeTest(testCase4, bugs, "Removed Five Biggest");

  return 0;
}
