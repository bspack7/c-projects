//
// Created by Brett Spackman on 2/22/18.
//

#include "RotationGame.h"
#include "Queue.h"

void RotationGame::bruteForceSolve(std::string fileName)
{
  id = 0;
  removedStates = 0;
  Board perfectBoard;
  perfectBoard.makeBoard();

  GameState one(id++);
  Board initial;
  initial.inputBoard(fileName);

//put board in queue
  Queue firstQueue;
  GameState initialState(id++);
  firstQueue.enqueue(initialState);
  std::cout << initialState.toString() << std::endl;


  int count = 0;
  bool won = false;

  while (!won) {
    GameState pull = firstQueue.dequeue();

    for (int i = 0; i < 12; i++) {
      GameState temp = pull;
      std::string catchit = pull.history + temp.board.move(i);
      GameState newState(id++);
      newState.board = temp.board;
      newState.history = catchit;
      newState.parentId = pull.id;
      firstQueue.enqueue(newState);
      std::cout << "Removed " << ++removedStates << " states" << std::endl << std::endl;
      std::cout << newState.toString() << std::endl;
      count++;
      if (newState.board.toString() == perfectBoard.toString()) {
        won = true;

        std::cout << "Removed " << ++removedStates << " states" << std::endl << std::endl;
        std::cout << newState.toString() << std::endl;
        std::cout << "Congrats you won!" << std::endl << std::endl;
        break;
      }
    }
  }
}

void RotationGame::aStarSolve(std::string fileName)
{
  id = 0;
  removedStates=0;
  Board perfectBoard;
  perfectBoard.makeBoard();

  GameState one(id++);
  Board initial;
  initial.inputBoard(fileName);
  one.board = initial;

  AvlTree<GameState> gState;
  gState.insert(one);

  while (one.board.board != perfectBoard.board) // may be an issue cause I initialize it as 0?
  {
    GameState min = gState.findMin();
    gState.removeMin();
    //std::cout << min.board.toString() << std::endl;
    std::cout << min;
    std::cout << "Removed " << ++removedStates << " states" << std::endl << std::endl;
    if (min.board.toString() == perfectBoard.toString())
    {
      std::cout<< "You won! " << std::endl;
      std::cout << min << std::endl;
      break;
    }
    for (int i = 0; i < 12; i++)
    {
      GameState newState = min;
      string move = newState.board.move(i);
      newState.history += move;
      newState.calcexpectedCost(perfectBoard);
      //update parent and my state
      newState.parentId = min.id;
      newState.id = id++;
      gState.insert(newState);
    }
  }
}