// -----------------------------------------------------------------------------
// GameState.h
// Nate Farnsworth
// Sp18 CS2420-002
// Program 1 - Rotation Puzzle
// -----------------------------------------------------------------------------

#pragma once
#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <sstream>
#include <string>
#include "Board.h"

using namespace std;

// A partial game. Member board must be specifically created after construction.
class GameState
{
public:
  int id;
  int parentId;
  Board board;
  int costSF = 0;
  int expectedCost = 0;
  int moves = 0;

  string history;

  GameState(int id, string history = "", int parentId = -1);
  GameState()
  {
    board.makeBoard();
  }
  string toString() const;

  /// ----------------------------------------- new ----------------------------------------------------------
  void calcexpectedCost(Board& perfectBoard);

  // overloading operators
  friend ostream&  operator<<( ostream& ss, const GameState& gs);

  bool operator > (const GameState & rhs) const
  {
    return expectedCost > rhs.expectedCost;
  }

  bool operator < (const GameState & rhs) const
  {
    return expectedCost < rhs.expectedCost;
  }

  bool operator >= (GameState & rhs) const
  {
    return expectedCost >= rhs.expectedCost;
  }

  bool operator <= (GameState & rhs) const
  {
    return expectedCost <= rhs.expectedCost;
  }
};



#endif