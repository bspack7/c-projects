
#ifndef QUEUE_H
#define QUEUE_H
#include "GameState.h"

class Queue
{
  struct Node
  {
    GameState currentState;
    Node *next = nullptr;
    Node(GameState startingState)
    {
      currentState = startingState;
    }
  };

  Node *front;
  Node *tail;

public:
  Queue();
  ~Queue();

  void enqueue(GameState currentBoard);
  GameState dequeue();
};

#endif