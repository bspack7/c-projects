//
// Created by Brett Spackman on 2/22/18.
//

#ifndef CS3HW3_ROTATIONGAME_H
#define CS3HW3_ROTATIONGAME_H

#include "GameState.h"
#include "AvlTree.h"

class RotationGame {
public:
  void bruteForceSolve(std::string fileName);
  void aStarSolve(std::string fileName);
private:
  int id = 0;
  int removedStates = 0;

};


#endif //CS3HW3_ROTATIONGAME_H
