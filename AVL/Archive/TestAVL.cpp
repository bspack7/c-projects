#include "AvlTree.h"
#include "GameState.h"
#include "RotationGame.h"
#include <iostream>
#include <iomanip>
#include <string>

int main()
{

RotationGame game;

  game.aStarSolve("input1.txt");
  game.bruteForceSolve("input1.txt");

  game.aStarSolve("input2.txt");
  game.bruteForceSolve("input2.txt");

  game.aStarSolve("input3.txt");
  game.bruteForceSolve("input3.txt");


  AvlTree<int> testTree;
  for (int i=1; i < 12; i=i+2)
  {
    testTree.insert(i);
  }
  testTree.insert(2);
  testTree.insert(4);
  testTree.insert(8);

  testTree.remove(7);
  testTree.remove(9);

  testTree.toString("Test Tree:");

  testTree.insert(50);
  testTree.insert(30);
  testTree.insert(15);
  testTree.insert(18);

  testTree.removeMin();
  testTree.toString("Test Tree:");

  testTree.removeMin();
  testTree.toString("Test Tree:");

  testTree.removeMin();
  testTree.toString("Test Tree:");

  testTree.insert(17);
  testTree.toString("Test Tree:");

//  AvlTree<int> tree;
//	for (int i=1; i < 30; i=i+3)
//	     tree.insert(i);
//	tree.insert(7);
//
//	tree.printTree();
//  tree.toString("Tree 1:");
//
//  std::cout << std::endl << tree.findMin() << std::endl;
//	std::cout << tree.findMax() << std::endl;
//	int check = 23;
//	if (!tree.contains(check))
//		std::cout << check << " ITEM_NOT_FOUND failed!" << std::endl;
//
//	std::string name = "this";
//
//	AvlTree<std::string> words;
//	words.insert("help");
//	words.insert("me");
//	words.insert("please");
//	words.insert("I");
//	words.insert("said");
//	words.insert("help");
//	words.insert("me");
//	words.insert("please");
//	words.printTree();
//
//  words.toString("Words Tree");

	return 0;
}
