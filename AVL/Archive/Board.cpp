// -----------------------------------------------------------------------------
// Board.h
// Nate Farnsworth
// Sp18 CS2420-002
// Program 1 - Rotation Puzzle
// -----------------------------------------------------------------------------

#include "Board.h"

// Constructor
Board::Board()
{
  makeBoard();
}

//Copy constructor
Board::Board(const Board& b)
{
  //cout << "Just Copied Board\n" << b.toString() << endl;
  assert(SIZE > 0);
  for (int i = 0; i < SIZE; i++)
  {
    for (int j = 0; j < SIZE; j++)
    {
      board[i][j] = b.board[i][j];
    }
  }
}

// Set value of board from file
void Board::inputBoard(string fileName)
{
  ifstream inputFile;
  inputFile.open(fileName.c_str());
  assert(inputFile.good());
  for (int i = 0; i < SIZE; i++)
  {
    for (int j = 0; j < SIZE; j++)
    {
      inputFile >> board[i][j];
    }
  }
}

// Randomly apply moves to the board
void Board::jumble(int count)
{
  srand(time(NULL));
  for (int i = 0; i < count; i++)
  {
    move(rand() % (SIZE * 4));
  }
}

// Create a board by performing legal moves on a board
// jumbleCt indicates the number of moves that may be required
// if jumbleCt ==0, return the winning board
void Board::makeBoard(int jumbleCount)
{
  int val = 1;
  for (int i = 0; i < SIZE; i++)
  {
    for (int j = 0; j < SIZE; j++)
    {
      board[i][j] = val++;
    }
  }

  jumble(jumbleCount);
}

// Make one move.  m indicates which move is wanted. Return string describing
// the move
string Board::move(int m)
{
  stringstream ss;
  //ss << ":";
  int sub = m / 4;
  switch (m % 4)
  {
    case 0:
      rotateNorth(sub); ss << "^" << sub;
      break;

    case 1:
      rotateSouth(sub); ss << "v" << sub;
      break;

    case 2:
      rotateEast(sub); ss << ">" << sub;
      break;

    case 3:
      rotateWest(sub); ss << "<" << sub;
      break;
  }

  return ss.str();
}

// Create a printable representation of the Board class
// The stringstream allows you to use the power of output commands in creating a string
string Board::toString() const
{
  stringstream ss;
  for (int i = 0; i < SIZE; i++)
  {
    for (int j = 0; j < SIZE; j++)
    {
      ss << board[i][j] << " ";
    }

    ss << endl;
  }

  return ss.str();
};

// Return true if board is identical to b
bool Board::operator==(Board &b)
{
  for (int i = 0; i < SIZE; i++)
  {
    for (int j = 0; j < SIZE; j++)
    {
      if (board[i][j] != b.board[i][j])
      {
        return false;
      }
    }
  }
  return true;
}

//Rotate East using row specified
void Board::rotateEast(int row)
{
  if (row < 0 || row >= SIZE)
  {
    return;
  }

  int wrap = board[row][SIZE - 1];
  for (int i = SIZE - 2; i >= 0; i--)
  {
    board[row][i + 1] = board[row][i];
  }

  board[row][0] = wrap;
}

//Rotate West using row specified
void Board::rotateWest(int row)
{
  if (row < 0 || row >= SIZE)
  {
    return;
  }

  int wrap = board[row][0];
  for (int i = 0; i < SIZE - 1; i++)
  {
    board[row][i] = board[row][i + 1];
  }

  board[row][SIZE - 1] = wrap;
}

//Rotate North using column specified
void Board::rotateNorth(int col)
{
  if (col < 0 || col >= SIZE)
  {
    return;
  }

  int wrap = board[0][col];
  for (int i = 0; i < SIZE - 1; i++)
  {
    board[i][col] = board[i + 1][col];
  }

  board[SIZE - 1][col] = wrap;
}

//Rotate South using column specified
void Board::rotateSouth(int col)
{
  if (col < 0 || col >= SIZE)
  {
    return;
  }

  int wrap = board[SIZE - 1][col];
  for (int i = SIZE - 2; i >= 0; i--)
  {
    board[i + 1][col] = board[i][col];
  }

  board[0][col] = wrap;
}
