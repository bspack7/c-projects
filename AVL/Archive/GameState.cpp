// -----------------------------------------------------------------------------
// GameState.cpp
// Nate Farnsworth
// Sp18 CS2420-002
// Program 1 - Rotation Puzzle
// -----------------------------------------------------------------------------

#include "GameState.h"

// Create a new gamestate with the supplied id, history string, and parent id
GameState::GameState(int newId, string newHistory, int newParentId)
{
  id = newId;
  history = newHistory;
  parentId = newParentId;
}

// Output gamestate data as a string
string GameState::toString() const
{
  stringstream ss;
  ss << "State " << id
     << " From State " << parentId << " "
     << history <<
                " ("
     << costSF << "):[" << expectedCost << "]" << endl;


  ss << board.toString();

  return ss.str();
}

/// ----------------------------------------- new functions ----------------------------------------------------------

void GameState::calcexpectedCost(Board& perfectBoard) // give it my current board
{
  expectedCost = 0;
  moves ++;
  // check to see if rows and columns match desired board
  // if it doesnt I know I need to make a move so increment expectedCost
  for (int i = 0; i<3; i++)
  {
    for (int j = 0; j<3; j++)
    {
      if (board.board[i][j] != perfectBoard.board[i][j])
      {
        expectedCost++;
      }
    }
  }
  expectedCost /= 3;
  expectedCost += moves;
}

ostream&  operator<<(ostream& ss, const GameState& gs) {
  ss << gs.toString();
  return ss;
}