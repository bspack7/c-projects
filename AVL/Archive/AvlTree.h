#pragma once
#ifndef AVL_TREE_H
#define AVL_TREE_H

#include <algorithm>
#include <iostream> 
#include <cassert>

// AvlTree class
//
// CONSTRUCTION: zero parameter
//
// ******************PUBLIC OPERATIONS*********************
// void insert( item )       --> Insert item
// void remove( item )       --> Remove item (unimplemented)
// bool contains( item )     --> Return true if item is present
// Comparable findMin( )  --> Return smallest item
// Comparable findMax( )  --> Return largest item
// boolean isEmpty( )     --> Return true if empty; else false
// void makeEmpty( )      --> Remove all items
// void printTree( )      --> Print tree in sorted order
// ******************ERRORS********************************
// Throws UnderflowException as warranted

template <typename Comparable>
class AvlTree
{
public:
	AvlTree() : root{ nullptr }
	{ }

	AvlTree(const AvlTree & rhs) : root{ nullptr }
	{
		root = clone(rhs.root);
	}

	AvlTree(AvlTree && rhs) : root{ rhs.root }
	{
		rhs.root = nullptr;
	}

	~AvlTree()
	{
		makeEmpty();
	}

	/**
	* Deep copy.
	*/
	AvlTree & operator=(const AvlTree & rhs)
	{
		AvlTree copy = rhs;
		std::swap(*this, copy);
		return *this;
	}

	/**
	* Move.
	*/
	AvlTree & operator=(AvlTree && rhs)
	{
		std::swap(root, rhs.root);

		return *this;
	}

	/**
	* Find the smallest item in the tree.
	* Abort if empty.
	*/
	const Comparable & findMin() const
	{
		assert(!isEmpty());
		return findMin(root)->element;
	}

	/**
	* Find the largest item in the tree.
	* Abortif empty.
	*/
	const Comparable & findMax() const
	{
		assert(!isEmpty());;
		return findMax(root)->element;
	}

	/**
	* Returns true if item is found in the tree.
	*/
	bool contains(const Comparable & item) const
	{
		return contains(item, root);
	}

	/**
	* Test if the tree is logically empty.
	* Return true if empty, false otherwise.
	*/
	bool isEmpty() const
	{
		return root == nullptr;
	}

	/**
	* Print the tree contents in sorted order.
	*/
	void printTree() const
	{
		if (isEmpty())
			std::cout << "Empty tree" << std::endl;
		else
			printTree(root);
	}

	/**
	* Make the tree logically empty.
	*/
	void makeEmpty()
	{
		makeEmpty(root);
	}

	/**
	* Insert item into the tree; duplicates are allowed
	*/
	void insert(const Comparable & item)
	{
		insert(item, root);
	}

	/**
	* Insert item into the tree; duplicates are allowed
	*/
	void insert(Comparable && item)
	{
		insert(std::move(item), root);
	}

	/**
	* Remove item from the tree. Nothing is done if item is not found.
	*/
	void remove(const Comparable & item)
	{
		remove(item, root);
	}

  /// ----------------------------------------- new functions ----------------------------------------------------------

  void  toString(std::string msg, int depth = std::numeric_limits<int>::max())
  {
    std::cout << std::endl << msg << std::endl;
    toString(root, "", 0, depth);
  }

  void removeMin()
  {
    removeMin(root);
    balance(root);
  }

private:
	struct AvlNode
	{
		Comparable element;
		AvlNode   *left;
		AvlNode   *right;
		int       height;

		AvlNode(const Comparable & ele, AvlNode *lt, AvlNode *rt, int h = 0)
			: element{ ele }, left{ lt }, right{ rt }, height{ h } { }

		AvlNode(Comparable && ele, AvlNode *lt, AvlNode *rt, int h = 0)
			: element{ std::move(ele) }, left{ lt }, right{ rt }, height{ h } { }
	};

	AvlNode *root;


	/**
	* Internal method to insert into a subtree.
	* item is the item to insert.
	* tree is the node that roots the subtree.
	* Set the new root of the subtree.
	*/
	void insert(const Comparable & item, AvlNode * & tree)
	{
		// std::cout << "insert &" << item << std::endl;
		if (tree == nullptr)
			tree = new AvlNode{ item, nullptr, nullptr };
		else if (item <= tree->element)
			insert(item, tree->left);
		else if (tree->element < item)
			insert(item, tree->right);

		balance(tree);
	}

	/**
	* Internal method to insert into a subtree.
	* item is the item to insert.
	* tree is the node that roots the subtree.
	* Set the new root of the subtree.
	*/
	void insert(Comparable && item, AvlNode * & tree)
	{
		// std::cout << "insert &&" << item << std::endl;
		if (tree == nullptr)
			tree = new AvlNode{ std::move(item), nullptr, nullptr };
		else if (item <= tree->element)
			insert(std::move(item), tree->left);
		else if (tree->element < item)
			insert(std::move(item), tree->right);

		balance(tree);
	}

	/**
	* Internal method to remove from a subtree.
	* item is the item to remove.
	* tree is the node that roots the subtree.
	* Set the new root of the subtree.
	*/
	void remove(const Comparable & item, AvlNode * & tree)
	{
		if (tree == nullptr)
			return;   // Item not found; do nothing

		if (item < tree->element)
			remove(item, tree->left);
		else if (tree->element < item)
			remove(item, tree->right);
		else if (tree->left != nullptr && tree->right != nullptr) // Two children
		{
			tree->element = findMin(tree->right)->element;
			remove(tree->element, tree->right);
		}
		else
		{
			AvlNode *oldNode = tree;
			tree = (tree->left != nullptr) ? tree->left : tree->right;
			delete oldNode;
		}

		balance(tree);
	}

	static const int ALLOWED_IMBALANCE = 1;

	// Assume t is balanced or within one of being balanced
	void balance(AvlNode * & tree)
	{
		if (tree == nullptr)
			return;

		if (height(tree->left) - height(tree->right) > ALLOWED_IMBALANCE)
			if (height(tree->left->left) >= height(tree->left->right))
				rotateWithLeftChild(tree);
			else
				doubleWithLeftChild(tree);
		else
			if (height(tree->right) - height(tree->left) > ALLOWED_IMBALANCE)
				if (height(tree->right->right) >= height(tree->right->left))
					rotateWithRightChild(tree);
				else
					doubleWithRightChild(tree);

		tree->height = max(height(tree->left), height(tree->right)) + 1;
	}

	/**
	* Internal method to find the smallest item in a subtree "tree".
	* Return node containing the smallest item.
	*/
	AvlNode * findMin(AvlNode *tree) const
	{
		if (tree == nullptr)
			return nullptr;
		if (tree->left == nullptr)
			return tree;
		return findMin(tree->left);
	}

	/**
	* Internal method to find the largest item in a subtree tree.
	* Return node containing the largest item.
	*/
	AvlNode * findMax(AvlNode *tree) const
	{
		if (tree != nullptr)
			while (tree->right != nullptr)
				tree = tree->right;
		return tree;
	}


	/**
	* Internal method to test if an item is in a subtree.
	* item is item to search for.
	* tree is the node that roots the tree.
	*/
	bool contains(const Comparable & item, AvlNode *tree) const
	{
		if (tree == nullptr)
			return false;
		else if (item < tree->element)
			return contains(item, tree->left);
		else if (tree->element < item)
			return contains(item, tree->right);
		else
			return true;    // Match
	}
	/****** NONRECURSIVE VERSION*************************
	bool contains( const Comparable & item, AvlNode *tree ) const
	{
	while( tree != nullptr )
	if( item < tree->element )
	tree = tree->left;
	else if( tree->element < item )
	tree = tree->right;
	else
	return true;    // Match

	return false;   // No match
	}
	*****************************************************/

	/**
	* Internal method to make subtree empty.
	*/
	void makeEmpty(AvlNode * & tree)
	{
		if (tree != nullptr)
		{
			makeEmpty(tree->left);
			makeEmpty(tree->right);
			delete tree;
		}
		tree = nullptr;
	}

	/**
	* Internal method to print a subtree rooted at "tree" in sorted order.
	*/
	void printTree(AvlNode *tree) const
	{
		if (tree != nullptr)
		{
			printTree(tree->left);
			std::cout  << tree->element << " ";
			printTree(tree->right);
		}
	}

	/**
	* Internal method to clone subtree.
	*/
	AvlNode * clone(AvlNode *tree) const
	{
		if (tree == nullptr)
			return nullptr;
		else
			return new AvlNode{ tree->element, clone(tree->left), clone(tree->right), tree->height };
	}
	// Avl manipulations
	/**
	* Return the height of node tree or -1 if nullptr.
	*/
	int height(AvlNode *tree) const
	{
		return tree == nullptr ? -1 : tree->height;
	}

	int max(int lhs, int rhs) const
	{
		return lhs > rhs ? lhs : rhs;
	}

	/**
	* Rotate binary tree node with left child.
	* For AVL trees, this is a single rotation for case 1.
	* Update heights, then set new root.
	*/
	void rotateWithLeftChild(AvlNode * & unhappyRoot)
	{
		AvlNode *temp = unhappyRoot->left;
		unhappyRoot->left = temp->right;
		temp->right = unhappyRoot;
		unhappyRoot->height = max(height(unhappyRoot->left), height(unhappyRoot->right)) + 1;
		temp->height = max(height(temp->left), unhappyRoot->height) + 1;
		unhappyRoot = temp;
	}

	/**
	* Rotate binary tree node with right child.
	* For AVL trees, this is a single rotation for case 4.
	* Update heights, then set new root.
	*/
	void rotateWithRightChild(AvlNode * & unhappyRoot)
	{
		AvlNode *temp = unhappyRoot->right;
		unhappyRoot->right = temp->left;
		temp->left = unhappyRoot;
		unhappyRoot->height = max(height(unhappyRoot->left), height(unhappyRoot->right)) + 1;
		temp->height = max(height(temp->right), unhappyRoot->height) + 1;
		unhappyRoot = temp;
	}

	/**
	* Double rotate binary tree node: first left child.
	* with its right child; then node k3 with new left child.
	* For AVL trees, this is a double rotation for case 2.
	* Update heights, then set new root.
	*/
	void doubleWithLeftChild(AvlNode * & superUnhappyRoot)
	{
		rotateWithRightChild(superUnhappyRoot->left);
		rotateWithLeftChild(superUnhappyRoot);
	}

	/**
	* Double rotate binary tree node: first right child.
	* with its left child; then node k1 with new right child.
	* For AVL trees, this is a double rotation for case 3.
	* Update heights, then set new root.
	*/
	void doubleWithRightChild(AvlNode * & superUnhappyRoot)
	{
		rotateWithLeftChild(superUnhappyRoot->right);
		rotateWithRightChild(superUnhappyRoot);
	}

  /// ----------------------------------------- new functions ----------------------------------------------------------

  //Print the contents of tree
  // Indent the tree by the string "indent"
  // Print the tree to a depth of "depth" given "currdepth" is the depth of tree
  void toString(AvlNode * tree, std::string indent, int currdepth = 0, int depth = std::numeric_limits<int>::max())
  {
    if (tree == NULL || currdepth>depth)
    {
      return;
    }
    toString(tree->right, indent + "  ", currdepth + 1, depth);
    std::cout << indent << tree->element << std::endl;
    toString(tree->left, indent + "  ", currdepth + 1, depth);
  }

  void removeMin (AvlNode * &tree)
  {
    if (tree == nullptr)
    {
      return;
    }
    if (root->left == nullptr)
    {
      AvlNode* temp = root->right;
      delete root;
      root = temp;
      return;
    }
    if (tree->left->left == nullptr)
    {
      if (tree->left->right != nullptr)
      {
        AvlNode* temp = tree->left->right;
        delete tree->left;
        tree->left = temp;
      }
      else
      {
        delete tree->left;
        tree->left = nullptr;
      }
      return;
    }
    removeMin(tree->left);
  }

};

#endif
