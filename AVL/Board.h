// -----------------------------------------------------------------------------
// Board.h
// Nate Farnsworth
// Sp18 CS2420-002
// Program 1 - Rotation Puzzle
// -----------------------------------------------------------------------------

#pragma once
#ifndef BOARD_H
#define BOARD_H

#include <cassert>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

// A single puzzle board and its provided functionality
class Board
{
private:
  static const int SIZE = 3;

  void rotateEast(int row);
  void rotateWest(int row);
  void rotateNorth(int col);
  void rotateSouth(int col);

public:
  int board[SIZE][SIZE];

  Board();
  Board(const Board& b);
  void inputBoard(string fileName);
  void jumble(int count);
  void makeBoard(int jumbleCount = 0);
  string move(int m);
  string toString() const;

  bool operator==(Board &b);
};

#endif
