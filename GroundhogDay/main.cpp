#include <iostream>
using namespace std;

int addition (int first, int second)
{
cout << "Enter your first number: \n";
cin >> first;
cout << "Enter your second number: \n";
cin >> second;
return first + second;
}

int substraction (int first, int second)
{
  cout << "Enter your first number: \n";
  cin >> first;
  cout << "Enter your second number: \n";
  cin >> second;
  if (second > first)
  {
    return second - first;
  }
  return (float)first - (float)second;
}

int mult (int first, int second)
{
  cout << "Enter your first number: \n";
  cin >> first;
  cout << "Enter your second number: \n";
  cin >> second;
  return first * second;
}

float divide (int first, int second)
{
  cout << "Enter your first number: \n";
  cin >> first;
  cout << "Enter your second number: \n";
  cin >> second;
  if (second > first)
  {
    return (float) second / first;
  }
  if (second == 0)
  {
  cout << "doesnt work! \n";
  }
  return (float) first / second;
}

int main() {

int whichFunction;

while (whichFunction == 1, 2, 3, 4)
{
  cout << "Choose a function: \n"
          "1. addition \n"
          "2. subtraction \n"
          "3. multiplication \n"
          "4. division \n";
  cin >> whichFunction;

  if (whichFunction == 1) {
    int sum = addition(5, 7);
    cout << "The sum of your two numbers is: " << sum << endl;
  }
  if (whichFunction == 2) {
    int difference = substraction(5, 7);
    cout << "The difference between your two numbers is: " << difference << endl;
  }
  if (whichFunction == 3) {
    int product = mult(5, 7);
    cout << "The product of your two numbers is: " << product << endl;
  }
  if (whichFunction == 4) {
    float quotient = divide(5, 7);
    cout << "The quotient of your two numbers is: " << quotient << endl;
  }
}
  return 0;
}