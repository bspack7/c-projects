#include "Person.h"

int main()
{
  Person personOne;

  personOne.setName("Henry the 8th");
  personOne.setAge(35);
  personOne.setGender('M');
  personOne.setHeight(72.56f);
  personOne.setWeight(260.501);

  personOne.printPerson();

  Person personTwo("J Jonah Jameson", 45, 'M', 250.09f, 72.5);

  personTwo.printPerson();

  return 0;
}