#ifndef PERSON_H
#define PERSON_H

#include <string>
#include <cstdlib>

using namespace std;

class Person
{
private:
  int age;
  string name;
  char gender;
  double weight;
  float height;

public:
  Person();
  Person(string newName, int newAge, char newGender, double newWeight, float newHeight);
  ~Person();

  int getAge()
  {
    return age;
  }

  string getName()
  {
    return name;
  }

  char getGender()
  {
    return gender;
  }

  double getWeight()
  {
    return weight;
  }

  float getHeight()
  {
    return height;
  }

  // your assignment is to write these function listed below
  // in another .cpp file
  void setName(string newName);
  void setAge(int newAge);
  void setGender(char newGender);
  void setWeight(double newWeight);
  void setHeight(float newHeight);

  void printPerson();

  void doABarrelRoll();
};

#endif