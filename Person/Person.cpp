#include <iostream>
#include "Person.h"
using namespace std;

Person::Person ()
{
  age = 0;
  name = "dude";
  gender = 'M';
  weight = 0.0;
  height = 0.0;
}

void Person::setName (string newName)
{
  name = newName;
}

void Person::setAge (int newAge)
{
  age = newAge;
}

void Person::setGender (char newGender)
{
  gender = newGender;
}

void Person::setWeight (double newWeight)
{
  weight = newWeight;
}

void Person::setHeight (float newHeight)
{
  height = newHeight;
}

void Person::printPerson()
{
  cout << "Name: \n" << name << "Age: \n" << age << "Gender: \n" << gender << "Weight: \n" << weight << "Height: \n" << height;
};

void Person::doABarrelRoll()
{
  name = "Yo Momma";
  age = 90;
  gender = 'M';
  weight = 9000.6;
  height = 7.5;
}

Person::Person(string newName, int newAge, char newGender, double newWeight, float newHeight) {

}

Person::~Person() {

}
